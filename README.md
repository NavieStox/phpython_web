# phpython

## initilization
First, you have to rename `dev_config.php` to `config.php`.
 ```sh
 $ mv dev_config.php config.php
 ```

## How to call python
For example, when you want to call `python/sample.py`,

```python:sample.py
def init(args=[]):
    data = {"txt": "hello, python"}
    print json.dumps(data)
```

then, insert a next code in your php file.

```php
python_exec(sample, array(), "json");
```
