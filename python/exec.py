import sys
import json
import traceback
from importlib import import_module


try:
    filename = sys.argv[1];
    if len(sys.argv)>2:
        args = sys.argv[2:]
    else:
        args = []

    module = import_module(filename)
    module.init(args)

except Exception as e:
    print("phpai/python Error:")
    print traceback.format_exc()
    print str(e)
