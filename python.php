<?php
    require_once "./config.php";


    function exec_python($filename,$args=array(),$data_type="json"){
        $code = "python ".ROOT_PATH."/python/exec.py '".$filename."'";
        foreach($args as $key => $value){
            $code .= " ".$value;
        }
        $out = shell_exec($code);
        if(preg_match("/^phpai\/python.*/", $out)){
            return $out;
        }else if($data_type=="json"){
            return json_decode($out,1);
        }else if($data_type=="text"){
            return $out;
        }
    }
?>
